Introduction
============

What is PicoRio
----------------
PicoRio is an open-source project stewarded by the `RISC-V International Open Source (RIOS) laboratory`_, a nonprofit research lab at Tsinghua-Berkeley Shenzhen Institute (TBSI). The RIOS Lab focuses on elevating the RISC-V software and hardware ecosystem collaboratively with both academia and industry. In PicoRio, we create an open, affordable, Linux-capable RISC-V hardware platform to help software developers port modern applications that require Javascript or GPUs. PicoRio will build upon high-quality IPs and software components contributed by experts from  industry and academia. PicoRio is not proprietary to any specific vendor or platform, and will have complete documentation that can help users build high-quality products in a short amount of time.

.. _RISC-V International Open Source (RIOS) laboratory: http://rioslab.org

Motivation
----------

A system is more than processors
  * Large cost to license other IPs in SoC: cache, interconnects, graphics, camera ISP, etc
  * An attractive open-source platform to experiment new hardware ideas
  * Full-system support is indispensable to security and trusted executions.
  * RISC-V hardware extensions: JIT runtime, vectorization, etc

The community lacks affordable RISC-V hardware platforms that is capable of executing diverse softwares
  * Few low-cost, software-capable boards for the long tail of developers
  * Developers won’t spend $1000 for a new hardware just for software development


Highlights
----------------

* **Independently Maintained**: The RIOS Lab is an independent nonprofit organization that governs the architecture development, ensures compliance, and will publish the design. The RIOS Lab will be the gatekeeper for both hardware and software, from SoC and firmware/drivers to high-level software and documentation. PicoRio will be vendor agnostic and non-proprietary. The RIOS Lab will work with academic and commercial organizations that will commit to its expansion and volume manufacturing.

* **Open Source**: PicoRio will open source as many components as possible, including the CPU and main SoC design, chip package, board design files, device drivers, and firmware. The exceptions are foundry related IPs (e.g., TSMC SRAM configurations), commercial high-speed interfaces, and complex commercial IP blocks like GPU. Nevertheless, our goal is to reduce the commercial closed source IPs for each successive release of PicoRio, with the long term goal of having a version that is as open as possible.

* **High-Quality IPs**: A major goal of the RIOS lab is developing open source, hardware IPs with industrial quality to boost the growth of RISC-V ecosystem and compete with those of existing, proprietary ISAs. Thus, PicoRio aims at a high-quality silicon release using open-source IPs. Such IPs will have gone through rigorous tapeout verifications that meet industry quality. The openness of PicoRio will not come at the cost of lower quality IP blocks. In addition, we will open source our verification process, which can further enhance transparency and trustworthiness.

* **Modern Software Stack Support**: PicoRio utilizes a heterogeneous multicore architecture and it is Linux-capable (RV64GC). We also designed PicoRio hardware to run modern managed languages such as JavaScript/WebAssembly as well as graphical applications like the Chrome web browser. In the RIOS Lab, PicoRio is also the hardware platform for several other open-source software projects, such as the RISC-V ports for the V8 Javascript engine and the Chromium OS.

* **Low-Power and Low-Cost**: The target metrics of PicoRio are low power dissipation and cost, which is a perfect match to the target of RISC-V system design.
