FAQ
==========


How is PicoRio compared to Raspberry Pi?
_________________________________________________________________________________________________________________


Inspired by the Raspberry Pi, we propose the PicoRio project, whose goal is to produce RISC-V based small-board computers at an affordable price point. PicoRio has differences in the following aspects:

* **Open Source**: Unlike Raspberry Pi, which uses proprietary Broadcom SoCs, PicoRio will open source as many components as possible, including the CPU and main SoC design, chip package and board design files, device drivers, and firmware. Nevertheless, our goal is to reduce the commercial closed source IPs for each successive release of PicoRio, with the long term goal of having a version that is as open as practical.

* **Low-Power and Low-Cost**: The target metrics of PicoRio are long battery life and low cost, which is a better match to RISC-V today, instead of high performance and large memory. In contrast, Raspberry Pi uses more power hungry ARM processors. For example, the idle power consumption has risen from 0.4 Watts to 2.7 Watts in the latest version of Raspberry Pi.
