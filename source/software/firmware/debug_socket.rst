=========================
Debug socket introduction
=========================

Debug-socket
============
Debug-socket is proxy running on host to interact with target, 
the functionality of debug-socket in software development, as 
shown in the following picture. 

.. figure:: ../../images/firmware/ds1.png
    :name: ds1

    Fig.1 Socket debug in SW development

According to the riscv-debug specification, if any kernel contains 
standard debug modules, simply follow the: “RISC-V external debugging 
support version xxx”. For standard debug module:

.. figure:: ../../images/firmware/ds2.png
    :name: ds2

    Fig.2 RISC-V debug overview

We choose to use a software-based debug socket instead of a standard 
debug module to implement the debug function, both of which have the 
same effect and can be used for debugging of the soc. For our debug-socket, 
see debug-socket connections overview.

.. figure:: ../../images/firmware/ds3.png
    :name: ds3

    Fig.3 Debug socket connection overview

Basically, the debug-socket implements basic functions required by gdb, 
with the help of hardware-provided breakpoint, watchpoint, trace buffer, 
and many other features.

Debug-socket supported command list
===================================
The full-stack debug tool development is under way, you can use the 
raw debug-socket interface to debug for now. Debug socket offers a big 
list of commands, however the following commands are the ones used most 
frequently:

==========================  ===============================     ==================================================
Command                     Usage                               Function
==========================  ===============================     ==================================================
b0                          b0 addr                             set a breakpoint at hw breakpoint 0 with addr
b1                          b1 addr                             set a breakpoint at hw breakpoint 1 with addr
b2                          b2 addr                             set a breakpoint at hw breakpoint 2 with addr
b3                          b3 addr                             set a breakpoint at hw breakpoint 3 with addr
d0                          d0                                  disable breakpoint at hw breakpoint 0
d1                          d1                                  disable breakpoint at hw breakpoint 1
d2                          d2                                  disable breakpoint at hw breakpoint 2
d3                          d3                                  disable breakpoint at hw breakpoint 3
wp (not supported for now)  wp                                  show watchpoint configure
bp                          bp                                  show breakpoint configure
c                           c                                   continue to run
stall                       stall                               make cpu stall
step N                      step N                              run next N instructions
gpr(not supported for now)  gpr                                 print all general purpose register
q                           q                                   quit debug-socket
wb_pc                       wb_pc                               show current excute instruction pc
if_pc                       if_pc                               show current fetch instruction pc
minstret                    minstret                            show m-mode excuted instruction count
mstatus                     mstatus                             show mstatus value
mcause                      mcause                              show mcause value
mepc                        mepc                                show mepc value
mip                         mip                                 show mip value
mie                         mie                                 show mie value
hpmcounter_3~hpmcounter_10  hpmcounter_3                        show PMU counter values
                            hpmcounter_4
                            hpmcounter_5
                            hpmcounter_6
                            hpmcounter_7
                            hpmcounter_8
                            hpmcounter_9
                            hpmcounter_10
dump                        dump 0x00f00000                     dump content from start address to end address
                            0x00f00080 rb/dma
read                        read 0x00f00000 rb/dma              read content from specified address, rb for 
                                                                device register & dma for memory
write                       write 0x00f00008 1 rb/dma           write value to specified address, rb for device 
                                                                register & dma for memory
uart1                       uart1                               show uart1 cfg
gpio                        gpio                                show gpio cfg
rtc                         rtc                                 show rtc cfg
wdt                         wdt                                 show wdt cfg
i2c0                        i2c0                                show i2c controller’s cfg
==========================  ===============================     ==================================================

Classical debug process
=======================
When encounter some error in program, you can use debug-socket to debug the program:

1. type ‘minstret’ twice to analysis if the CPU is stall or not, if the two values 
of minstret is the same value, the CPU is stalled

::

    : minstret
    Do Read to Addr 0x1002b0 (minstret), Got Data 0x2409734f
    Please enter command: (All Data in HEX no matter 0x is added or not)
    : minstret
    Do Read to Addr 0x1002b0 (minstret), Got Data 0x240aa177
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :

2. if the CPU is not stalled, type 'wb_pc'

::

    : wb_pc
    Do Read to Addr 0x100258 (wb_pc), Got Data 0x80009430
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :

3. use 'b0 addr' to set a breakpoint, the program will stop when run into addr

::

    : b0 80008e48
    add breakpoint0, pc_addr = 0x80008e48
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :

4. then, you can use 'read addr dma' to check some var value

::

    : read 800102c4 dma
    Do Read to Addr 0x800102c4, Got Data 0x6ffffffff
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :
5. type 'step N' to run N instructions

::

    : step 10
    pc = 0x80000300
    pc = 0x80000304
    pc = 0x80000308
    pc = 0x8000030c
    pc = 0x80000310
    pc = 0x80000314
    pc = 0x80000318
    pc = 0x8000031c
    pc = 0x80000320
    pc = 0x80000324
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :
6. re-check some var

::

    : read 800102c4 dma
    Do Read to Addr 0x800102c4, Got Data 0x6ffffffff
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :
7. continue to run until run into the breakpoint again

::

    : c
    Continue
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :
8. disable breakpoint

::

    : d0
    del hw breakpoint1
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :
9. continue

::

    : c
    Continue
    Please enter command: (All Data in HEX no matter 0x is added or not)
    :