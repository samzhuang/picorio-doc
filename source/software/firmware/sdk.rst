==========================
ES1Y SDK v1.0 Introduction
==========================
ES1Y Software Development Kit is used in linux platform at present 
and it will support much more host OS, e.g. windows later. The SDK 
provides freertos APIs for customers' application development, 
what's more, there are some system test demos included in the SDK 
so as to help the new customers get on hand quickly.

1. Getting start
================

- In this chapter, we need prepare development environment and know how to make the binary running on ES1Y SoC, and then rvSDK provide straightforward tools for debugging. 

    1. Init SDK 
    
        Follow the README.md file at SDK v1.0 root dir firstly.
        
    2. Compile & Run
    
    - Build the gcc toolchain
        ::

            $ cd build
            $ make gcc
    - Build the fesvr & debug proxy
        ::

            $ make fesvr
            $ make driver
    - Build freertos and application code
        ::

            # clean if needed
            $ make freertos-clean
            $ make freertos
    - Run vivado to use FPGA as a debug tool, at the same time, run debug proxy
        ::

            # this command only need execute once time in the whole debug process
            $ make run-vivado # or shortly 'make rv'
    - Download and run FreeRTOS firmware through debug proxy
        ::

            $ make run-rtos # or shortly 'make rvt'
            # when you finish your debug and want to exit
            # use Ctrl+C Ctrl+C(that is: input Ctrl+C twice).
    - You can use this one command below instead of steps above to simplify build process
        ::

            $ make freertos-all
    - One additional command is provided to speed up debuging after edit source code
        ::

            # this command equal to make freertos && make run-rtos
            $ make re-comp-run-rtos # or shortly 'make rvrt'
    - The default code in rvSDK v1.0 will startup two tasks, which one print 'TEST' per second and the other one print 'DEMO’ every 2 seconds, after do some IO test:
        ::

            *************************************************

            Welcome enter FreeRTOS on pygmy_e platform

            *************************************************

            TEST DEMO for IO functions ...

            TEST IO functions done ...
            ------- TEST -------

            Demo task ...
            ------- Demo -------
            ------- TEST -------
            ------- TEST -------
            ------- Demo -------
            ------- TEST -------
            ------- TEST -------
            ------- Demo -------

    3. Debug
        - Console by UART
            - Please read the other document that introduces usb-uart dongle connection between host & target.
            - The proper UART baudrate & other configuration is 500000, 8n1 for print debugging.
        - Command Line Interface(CLI)
            - There are some limitations for debugging CLI in rvSDK v1.0 with debug-spi-base.o, anyway, we will provide more abundant debugging tool in future. 
                1. Debug tool startup interface
                ::
                    
                    $ cd software/host/driver/pygmy_e
                    $ ./debug-socket.o
                    serverPort = 8800
                    Please enter command: (All Data in HEX no matter 0x is added or not)
                    : 
                2. read the current pc value
                ::

                    : if_pc
                    Do Read to Addr 0x100238 (if_pc), Got Data 0x8000943c
                    Please enter command: (All Data in HEX no matter 0x is added or not)
                    : wb_pc
                    Do Read to Addr 0x100258 (wb_pc), Got Data 0x80009430
                    Please enter command: (All Data in HEX no matter 0x is added or not)
                    :
                3. read device register
                ::

                    : read 80bff8 rb
                    Do Read to Addr 0x80bff8, Got Data 0xbccb5ade85
                    Please enter command: (All Data in HEX no matter 0x is added or not)
                    : 
                4. read memory
                ::

                    : read 8000f798 dma
                    Do Read to Addr 0x8000f798, Got Data 0x20656e6f6420736e
                    Please enter command: (All Data in HEX no matter 0x is added or not)
                    :
            - Reference debug-socket introduction for more debug commands introduction.

2. How to code
==============
    - In rvSDKv1/target/src/Demo/pymgy_e/, app_entry.c is main application entry c file, and you should implement rvHalCB_app_entry() within it depend on your requirement.
    - By default, run demo program if enable #define APP_SYSTEM_TEST in app_entry.c, otherwise run your program.
    - The demo code in this file which supports i2c, gpio and spi flash. (controlled by #define TEST_DEMO_GPIO #define TEST_DEMO_I2C #define TEST_DEMO_SPI_FLASH in target/src/Demo/pymgy_e/hal/config/pygmy_e/system_config.h)
    - Anyway, you can implement rvHalCB_app_entry() by your own requirement

3. Programming API
==================
    - Freertos API 
        - we can easily get the help from https://www.freertos.org/FreeRTOS-quick-start-guide.html 