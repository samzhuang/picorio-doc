Software Projects
=================

This section describes the software projects which PicoRio supports. We put all projects in a dashboard, and list out the current developing status of them.


.. toctree::
   :hidden:
   :maxdepth: 2
  
   ./software/firmware
   ./software/v8.wiki/Home
	     

+-------------------+------------------------------+----------------------------------------------------+---------------------------------------------+
| Projects          |     Development State        |     Project Description                            |  Link                                       |
+===================+==============================+====================================================+=============================================+
| `Firmware`_       |                              | ES1Y Firmware includes Debug socket, ES1Y SDK and  | https://gitlab.com/picorio/picorio-software |
|                   |                              | ES1Y API.                                          |                                             |
+-------------------+------------------------------+----------------------------------------------------+---------------------------------------------+
| `V8`_             |                              | V8 is a commonly used JavaScript engine in         | https://github.com/v8-riscv/v8/             |
|                   |                              | popular web browsers. PicoRio provides             |                                             |
|                   |                              | support for RISC-V V8.                             |                                             |
+-------------------+------------------------------+----------------------------------------------------+---------------------------------------------+
| Chromium OS       |                              | Chromium OS is a open-source web browser           |                                             |
|                   |                              | with strong web application support and rich       |                                             |
|                   |                              | software ecosystem. This project is RISC-V         |                                             |
|                   |                              | port of Chromium OS, and is in development.        |                                             |
+-------------------+------------------------------+----------------------------------------------------+---------------------------------------------+


.. _Firmware: ./software/firmware.html
.. _V8: ./software/v8.wiki/Home.html
