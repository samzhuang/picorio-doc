========
L1 Cache
========

L1 Data Cache
=============

As part of the memory hierarchy, the L1 data cache helps cut down memory access time of cpu.
In that the L1 D-Cache is private, the cache coherence among multicores is a major problem to settle.
The design and implementation of cache coherent scheme and other design details are work
in progress.

Parameter
^^^^^^^^^

The parameter of L1 data cache is as follows:

  ======================   ==================   ===================   =====================
  Cache capacity           Cache line numbers   Cache line capacity   Mapping method
  ======================   ==================   ===================   =====================
  32 KBytes                512                  32 Bytes              2-way set associative
  ======================   ==================   ===================   =====================

L1 Instruction Cache
====================

As part of memory hierarchy, the L1 instruction cache helps cut down the latency of cpu instruction fetching.

The parameter of L1 instruction cache is as follows:

  ======================   ==================   ===================   =====================
  Cache capacity           Cache line numbers   Cache line capacity   Mapping method
  ======================   ==================   ===================   =====================
  8 KBytes                 128                  32 Bytes              2-way set associative
  ======================   ==================   ===================   =====================
