===================================
Overview
===================================
RRV64 is a 64-bit RISC-V Core designed for embedded applications.
It has a 5 stage in-order pipeline and multi-level cache system including
L1 and L2 I/D caches. RRV64 supports RV64IMAC instruction sets,
Sv39 Virtual Address format, legal combinations of privilege modes in
conjunction with Physical Memory Protection (PMP). It is capable of running
a full-featured operating system like Linux. The core is compatible with all
applicable RISC‑V standards.

RRV64 is designed to be feature a very flexible memory system that includes L1 caches, L2 caches, bus interfaces, and memory maps that provide a lot of flexibility for SoC integration.


    .. figure:: ../../images/core_overview.png
      :name: core_overview_img

      Fig. 1 Core Overview

Fig. 1 illustrates a simplified RRV64 pipeline

===================================
Repository Organization
===================================
The following shows the main folders in RRV64 repository and their usage::

    .
    |─rtl                   --RRV64 RTL description using SystemVerilog code
    │  ├─common             --Macro and parameter definition files
    │  ├─lib                --Components used in RRV64, such as FIFO, RAM, etc
    │  └─rrv64              --RRV64 Core
    └─tb                    --Benchmarks, testbenchs and Makefile for simulation
      ├─rrv64               --The testbench of the top-level module for simulation
      ├─perfect_mem_model   --The testbench of an ideal L2Cache
      └─test_program        --Benchmarks for testing the CPU
          └─benchmarks
